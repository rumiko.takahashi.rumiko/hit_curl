
#include "../inc/hitbtc.h"
#include <curses.h>
#include <ncurses.h>

int 		first_part(t_json *json, int j, t_search_param	*user, int *k)
{
    int flag;

    flag = 0;
    if (json->open_value[j] == 0 || json->last_value[j] == 0)
      json->change2[j] = 0;
    else
    {
          json->change2[j] = (json->last_value[j] - json->open_value[j]) / (json->open_value[j] / 100);
    }
    if (user->pr_bl1 >= 0 && json->change2[j] >= user->pr_bl1 && input_check_1(json->symbol_name[j], user)
        && !out_check_1(json->symbol_name[j], user))
    {
      flag = 1;
      json->f1_reset[k[0]] = ft_strdup(json->symbol_name[j]);
      k[0] += 1;
      printf("%s\t%s > %.10f  %s\n", GRN, json->symbol_name[j] ,json->change2[j], RESET);
      printf("     %.10f --- %.10f\n",
            json->open_value[j],
            json->last_value[j]);
    }
    if (user->pr_bl1 < 0 && json->change2[j] <= user->pr_bl1 && input_check_1(json->symbol_name[j], user)
        && !out_check_1(json->symbol_name[j], user))
    {
        flag = 1;
        json->f1_reset[k[0]] = ft_strdup(json->symbol_name[j]);
        k[0] += 1;
        printf("%s\t%s > %.10f  %s\n", RED, json->symbol_name[j] ,json->change2[j], RESET);
        printf("     %.10f --- %.10f\n",
            json->open_value[j],
            json->last_value[j]);
      }
  return (flag);
}

int   input_check_1(char *str, t_search_param	*user)
{
  int   j;

  j = 0;
  if (strcmp(user->search_value_1[0], "ALL") == 0)
    return (1);
  while (user->search_value_1[j] != NULL)
  {
    if (strcmp(str, user->search_value_1[j]) == 0)
    {
      printf("%s\n", str);
       return (1);
    }
    j++;
  }
  if (j == 0)
    return (1);
return (0);
}

int   out_check_1(char *str, t_search_param	*user)
{
  int   j;

  j = 0;
  if (strcmp(user->search_value_1[0], "NONE") == 0)
    return (1);
  while (user->unsearch_value_1[j] != NULL)
  {
    if (strcmp(str, user->unsearch_value_1[j]) == 0)
       return (1);
    j++;
  }
  if (j == 0)
    return (1);
return (0);
}
