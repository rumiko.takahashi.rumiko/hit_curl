
#include "../inc/hitbtc.h"

void	put_param_bl1(char *str, t_search_param	*user)
{
	int i;

	i = 0;
	printf("%s%s%s%s", "Введите ", YEL, "ПРОДОЛЖИТЕЛЬНОСТЬ ЗВУКОВОГО СИГНАЛА - ", RESET);
	printf("%s", "время в минутах, по истечению которого программа возобновит ");
	printf("%s", "свою работу. Например, 3 или 5.\n");
	scanf("%s", str);
	user->beep_time = ft_strdup(str);
	if (user->time == 0)
	{
		while (user->beep_time[i] != '\0')
		{
			if (user->beep_time[i] < '0' || user->beep_time[i] > '9')
			{
				printf("%s%s%s\n", RED, "Введено некорректное значение!", RESET);
				exit(1);
			}
			i++;
		}
	}
	ft_bzero(str, 1024);

	printf("%s%s%s%s", "Введите ", YEL, "ПЕРИОДИЧНОСТЬ ", RESET);
	printf("%s\n", "обновления информации в секундах. Например, 30\n");
	scanf("%s", str);
	user->time = atoi(str);
	if (user->time == 0)
	{
		printf("%s%s%s\n", RED, "Введено некорректное значение!", RESET);
		exit(1);
	}
	ft_bzero(str, 1024);

	printf("%s%s%s%s", "Введите отслеживаемый", YEL, " ПРОЦЕНТ", RESET);
	printf("%s", " изменений с учетом знака для блока 1. Например значение <25> ");
	printf("%s", "будет остлеживать изменения сторону роста курса монетки, а значение <-30> ");
	printf("%s\n", "отслеживать снижение стоимости.");
	scanf("%s", str);
	user->pr_bl1 = atoi(str);
	if (user->pr_bl1 == 0)
	{
		printf("%s%s%s\n", RED, "Введено некорректное значение!", RESET);
		exit(1);
	}
	ft_bzero(str, 1024);

	printf("%s%s%s%s", "Введите ", YEL, "названия ", RESET);
	printf("%s%s%s%s", "отслеживаемых пар в блоке ", GRN, "1 ", RESET);
	printf("%s%s%s%s%s", "или ", YEL, "ALL ", RESET, "и нажмите клавишу Enter. ");
	printf("%s%s%s%s", "НАЗВАНИЯ вводить в формате ", GRN, "ETHBTC,XEMBTC", RESET);
	printf("%s", ". Без пробелов внутри торговой пары! Торговые пары разделены между собой символом <");
	printf("%s%s%s%s\n", GRN, ",", RESET, ">!!!");
	scanf("%s", str);
	user->search_value_1 = ft_strsplit(str, ',');
	ft_bzero(str, 1024);

	user->unsearch_value_1 = read_file_1();

}

void	put_param_bl2(char *str, t_search_param	*user)
{
	int i;

	i = 0;
	if (user->reg == 2)
	{
		printf("%s%s%s%s", "Введите ", YEL, "ПРОДОЛЖИТЕЛЬНОСТЬ ЗВУКОВОГО СИГНАЛА - ", RESET);
		printf("%s", "время в минутах, по истечению которого программа возобновит ");
		printf("%s", "свою работу. Например, 3 или 5.\n");
		scanf("%s", str);
		user->beep_time = ft_strdup(str);
		if (user->time == 0)
		{
			while (user->beep_time[i] != '\0')
			{
				if (user->beep_time[i] < '0' || user->beep_time[i] > '9')
				{
					printf("%s%s%s\n", RED, "Введено некорректное значение!", RESET);
					exit(1);
				}
				i++;
			}
		}
		ft_bzero(str, 1024);

		printf("%s%s%s%s", "Введите ", YEL, "ПЕРИОДИЧНОСТЬ ", RESET);
		printf("%s\n", "обновления информации в секундах. Например, 30\n");
		scanf("%s", str);
		user->time = atoi(str);
		if (user->time == 0)
		{
			printf("%s%s%s\n", RED, "Введено некорректное значение!", RESET);
			exit(1);
		}
		ft_bzero(str, 1024);
	}

	printf("%s%s%s%s", "Введите отслеживаемый", YEL, " ПРОЦЕНТ", RESET);
	printf("%s", " изменений с учетом знака для блока 2. Например значение <25> ");
	printf("%s", "будет остлеживать изменения сторону роста курса монетки, а значение <-30> ");
	printf("%s\n", "отслеживать снижение стоимости.");
	scanf("%s", str);
	user->pr_bl2 = atoi(str);
	if (user->pr_bl2 == 0)
	{
		printf("%s%s%s\n", RED, "Введено некорректное значение!", RESET);
		exit(1);
	}
	ft_bzero(str, 1024);

	printf("%s%s%s%s", "Введите ", YEL, "названия ", RESET);
	printf("%s%s%s%s", "отслеживаемых пар в блоке ", GRN, "2 ", RESET);
	printf("%s%s%s%s%s", "или ", YEL, "ALL ", RESET, "и нажмите клавишу Enter. ");
	printf("%s%s%s%s", "НАЗВАНИЯ вводить в формате ", GRN, "ETHBTC,XEMBTC", RESET);
	printf("%s", ". Без пробелов внутри торговой пары! Торговые пары разделены между собой символом <");
	printf("%s%s%s%s\n", GRN, ",", RESET, ">!!!");
	scanf("%s", str);
	user->search_value_2 = ft_strsplit(str, ',');
	ft_bzero(str, 1024);

	user->unsearch_value_2 = read_file_2();
}

void	init_user_struct(t_search_param	*user)
{
	user->reg = 0;
	user->time = 0;
	user->pr_bl1 = 0;
	user->pr_bl2 = 0;
	user->beep_time = NULL;
	user->search_value_1 = NULL;
	user->search_value_2 = NULL;
	user->unsearch_value_1 = NULL;
	user->unsearch_value_2 = NULL;
}

void	test_print_func(t_search_param	*user)
{
	int i;

	if (user->reg == 1)
	{
		printf("Bepp_time = %s\n", user->beep_time);
		printf("Update_time = %d\n", user->time);
		printf("Search_valume1 = %d\n", user->pr_bl1);
		i = 0;
		while (user->search_value_1[i])
			printf("Search_monets1 = %s\n", user->search_value_1[i++]);
		i = 0;
		while (user->unsearch_value_1[i])
			printf("NOT search_monets1 = %s\n", user->unsearch_value_1[i++]);
	} else if (user->reg == 2)
	{
		printf("Bepp_time = %s\n", user->beep_time);
		printf("Update_time = %d\n", user->time);
		printf("Search_valume2 = %d\n", user->pr_bl2);

		i = 0;
		while (user->search_value_2[i])
			printf("Search_monets2 = %s\n", user->search_value_2[i++]);
		i = 0;
		while (user->unsearch_value_2[i])
			printf("NOT search_monets2 = %s\n", user->unsearch_value_2[i++]);
	}
	else
	{
		printf("Bepp_time = %s\n", user->beep_time);
		printf("Update_time = %d\n", user->time);
		printf("Search_valume1 = %d\n", user->pr_bl1);
		i = 0;
		while (user->search_value_1[i])
			printf("Search_monets1 = %s\n", user->search_value_1[i++]);
		i = 0;
		while (user->unsearch_value_1[i])
			printf("NOT search_monets1 = %s\n", user->unsearch_value_1[i++]);

		printf("%s\n", "________________________________________________________________________");

		printf("Search_valume2 = %d\n", user->pr_bl2);

		i = 0;
		while (user->search_value_2[i])
			printf("Search_monets2 = %s\n", user->search_value_2[i++]);
		i = 0;
		while (user->unsearch_value_2[i])
			printf("NOT search_monets2 = %s\n", user->unsearch_value_2[i++]);
	}
}

void	put_param(t_search_param	*user)
{
	char *str;
	str = malloc(sizeof(char) * 1024);
	init_user_struct(user);
	printf("%s%s %s\n", GRN, "\n\tПрограмма для отслеживания значений HitBTC запущена!\n", RESET);
	printf("%s", "Чтобы запустить режим отслеживания изменений в блоке ");
	printf("%s %s %s %s %s\n", YEL, "CHANGE", RED, "введите 1" RESET, "и нажмите Enter");
	printf("%s", "Чтобы запустить режим отслеживания изменений в блоке ");
	printf("%s %s %s %s %s\n", YEL, "PRICE", RED, "введите 2" RESET, "и нажмите Enter");
	printf("%s", "Чтобы запустить оба режима отслеживания изменений");
	printf("%s %s %s %s\n", RED, "введите 3", RESET, "и нажмите Enter");

	scanf("%s", str);
	user->reg = atoi(str);
	{
		if (user->reg < 1 || user->reg > 3)
		{
			printf("%s%s%s\n", RED, "Введено некорректное значение!", RESET);
			exit(1);
		}
	}

	ft_bzero(str, 1024);
	if (user->reg == 1)
		put_param_bl1(str, user);
	else if (user->reg == 2)
		put_param_bl2(str, user);
	else if (user->reg == 3)
	{
		put_param_bl1(str, user);
		put_param_bl2(str, user);
	}

	test_print_func(user);
	free(str);
	str = NULL;
}
