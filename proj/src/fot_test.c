/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/17 20:07:11 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/28 14:58:52 by vbudnik          ###   ########.fr       */
/*   Updated: 2018/05/27 22:39:34 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/hitbtc.h"
#include <fcntl.h>

void  test_secondpart(int count, char **str)
{
  int     fd;
  char    *line;
  char    *for_open;

  if (!(for_open = (char *)malloc(sizeof(char) * 1024)))
    return ;
  strcpy(for_open, "test_second_part/");
  strcat(for_open, ft_itoa(count));
  if ((fd = open(for_open, O_RDONLY)) < 0)
  {
    printf("PROBLEM WITH opening file, check name\n");
    exit (0);
  }
  if (fd > 0)
    get_next_line(fd, &line);
  if (fd > 0)
    close(fd);
  if (line == NULL)
    return ;
  str[0] = strdup(line);
  free(line);
}
