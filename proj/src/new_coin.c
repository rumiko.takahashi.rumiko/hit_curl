
#include "../inc/hitbtc.h"
#include <curses.h>
#include <ncurses.h>

int   find_coint(t_json *json)
{
  int   i;
  int   j;
  int   n;
  int   flag;

  i = 0;
  j = 0;
  flag = 0;
  n = 0;
  while(json->symbol_name[i] != NULL)
  {
    j = 0;
    flag = 0;
    while (json->last_symbol[j] != NULL)
    {
      if (strcmp(json->last_symbol[j], json->symbol_name[i]) == 0)
      {
        flag = 1;
        break ;
      }
      j++;
    }
    if (flag == 0)
    {
      printf("\n== new coin == \n");
      printf("****** %s *****\n", json->symbol_name[i]);
      n++;
    }
    i++;
  }
  if (n)
    usleep(40);
  return (n);
}

void    swap_symbol(t_json *json)
{
  int     i;

  i = 0;
  while(json->symbol_name[i] != NULL)
  {
    json->last_symbol[i] = strdup(json->symbol_name[i]);
    i++;
  }
  json->last_symbol[i] = NULL;
}
