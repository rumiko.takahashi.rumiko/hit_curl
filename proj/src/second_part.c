
#include "../inc/hitbtc.h"
#include <curses.h>
#include <ncurses.h>

int 		second_part(t_json *json, int j, t_search_param	*user, int *k, int c)
{
  int flag;
  int index;

  index = 0;
  flag = 0;
  if (json->last_value[j] == 0 || json->const_value[j] == 0)
      json->change[j] = 0;
  if (c >= 1)
  {
    index = serch(json->symbol_name[j], json); // поиск правильного индекса
    json->change[j] = (json->last_value[j] - json->const_value[index]) / (json->const_value[index] / 100);
  }
  else
  {
    json->change[j] = (json->last_value[j] - json->const_value[j]) / (json->const_value[j] / 100);
  }
  if (user->pr_bl2 >= 0 && json->change[j] >= user->pr_bl2 && input_check_2(json->symbol_name[j], user)
    && !out_check_2(json->symbol_name[j], user) && index >= 0)
  {
      flag = 1;
      json->f2_reset[k[0]] = ft_strdup(json->symbol_name[j]);
      k[0] += 1;
       printf("%s\t\t\t\t\t\t\t%s > %.10f  %s\n", YEL, json->symbol_name[j] ,json->change[j], RESET);
       printf("\t\t\t\t\t\t\tconst %.10f --- now %.10f\n",
              json->const_value[index],
              json->last_value[j]);

  }
  if (user->pr_bl2 < 0 && json->change[j] <= user->pr_bl2 && input_check_2(json->symbol_name[j], user)
      && !out_check_2(json->symbol_name[j], user))
  {
    flag = 1;
      json->f2_reset[k[0]] = ft_strdup(json->symbol_name[j]);
      k[0] += 1;
      printf("%s\t\t\t\t\t\t\t%s > %.10f  %s\n", MAG, json->symbol_name[j] ,json->change[j], RESET);
      printf("\t\t\t\t\t\t\t%.10f --- %.10f\n",
              json->const_value[index],
              json->last_value[j]);
    }
  return (flag);
}


int   input_check_2(char *str, t_search_param	*user)
{
  int   j;

  j = 0;
  if (strcmp(user->search_value_2[0], "ALL") == 0)
    return (1);
  while (user->search_value_2[j] != NULL)
  {
    if (strcmp(str, user->search_value_2[j]) == 0)
       return (1);
    j++;
  }
return (0);
}

int   out_check_2(char *str, t_search_param	*user)
{
  int   j;

  j = 0;
  if (strcmp(user->search_value_2[0], "NONE") == 0)
    return (1);
  while (user->unsearch_value_2[j] != NULL)
  {
    if (strcmp(str, user->unsearch_value_2[j]) == 0)
       return (1);
    j++;
  }
return (0);
}

int   serch(char *str, t_json *json)
{
  int   i;
  int   j;

  j = 0;
  i = 0;
  while (i < json->len_privius / 10)
  {
    if (strcmp(str, json->const_symbol_name[i]) == 0)
      return (i);
    i++;
  }
  return (-1);
}
