/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/28 15:13:52 by vbudnik           #+#    #+#             */
/*   Updated: 2018/05/28 15:13:54 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/hitbtc.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../inc/hitbtc.h"

void 		beeper_ask(char *str, int f1, int f2, t_json *json, t_search_param	*user)
{
		char		c[1024];
		time_t 	start_time;
		time_t	finish_time;
		int			delta;
		int 		i;
		int			k;
		int			prepea_time;

		prepea_time = atoi(str);
		prepea_time *= 60;
		start_time = time(NULL);
		i = BMW(str);
		if (i != 0)
			exit(0);
		finish_time = time(NULL);
		delta = (int)finish_time - (int)start_time;
		if (delta < prepea_time)
		{
			while (1)
			{
				printf("Хотите исключить найденные монеты из списка отслеживаемых? Введите ");
				printf("Y для исключения, N для продолжения работы программы,");
				printf(" Q для выхода из программы!\n");
				scanf("%s", c);
				if (c[0] == 'Y' || c[0] == 'y')
				{
					if (f1)
					{
						k = 0;
						user->unsearch_value_1 = copy_array_glob(json->f1_reset, user->unsearch_value_1);
						while (user->unsearch_value_1[k] != NULL)
							write_file_1(user->unsearch_value_1[k++]);
					}
					if (f2)
					{
						k = 0;
						user->unsearch_value_2 = copy_array_glob(json->f2_reset, user->unsearch_value_2);
						while (user->unsearch_value_2[k] != NULL)
							write_file_2(user->unsearch_value_2[k++]);
					}
					break ;
				}
				if (c[0] == 'N' || c[0] == 'n')
				{
					if (f1)
					{
						ft_freearr(json->f1_reset);
						free(json->f1_reset);
						json->f1_reset = NULL;
					}
					if (f2)
					{
						ft_freearr(json->f2_reset);
						free(json->f2_reset);
						json->f2_reset = NULL;
					}
					break ;
				}
				if (c[0] == 'Q' || c[0] == 'q')
					exit(0);
				printf("Только указанные значения!!!\n");
			}
		}
}

void	signal_handler(int sig)
{
	if (sig == SIGINT)
	{
		printf("\n\nСпеши купить билеты в ОАЭ\n");
		signal(SIGINT, signal_handler);
	}
}

int	BMW(char *arg)
{
	pid_t	pid;
	int		sig;
	char	**tmp;
	char	**env;
	extern char	**environ;
	int			len;
	int			i;
	int 		j;

	i = 0;
	j = 0;
	len = 0;
	while (environ[len] != NULL)
		len++;
	env = (char **)malloc(sizeof(char *) * (len + 1));
	while (environ[i] != NULL)
	{
		env[i] = strdup(environ[i]);
		i++;
	}
	env[i] = NULL;
	tmp = (char **)malloc(sizeof(char *) * 4);
	tmp[0] = strdup("./beep");
	tmp[1] = strdup(arg);
	tmp[2] = NULL;
	i = 0;
	signal(SIGINT, signal_handler);
	pid = fork();
	if (pid < 0)
		;
	if (pid > 0)
		wait(&sig);
	if (pid == 0)
		i = execve(tmp[0], tmp, env);
	while (--len >= 0)
		free(env[len]);
	while (j < 3)
		free(tmp[j++]);
	free(env);
	env = NULL;
	free(tmp);
	tmp = NULL;
	return (i);
}

char		*ft_argsjoin(char **args)
{
	int		i;
	char	*str;
	char	*tmp;

	i = 0;
	str = "\0";
	tmp = "\0";
	while (args[i])
	{
		str = ft_strjoin(tmp, args[i]);
		if (i > 0)
			free(tmp);
		if (i < (ft_len2darr(args)))
		{
			tmp = str;
			str = ft_strjoin(tmp, " ");
			free(tmp);
		}
		tmp = str;
		i++;
	}
	return (str);
}
