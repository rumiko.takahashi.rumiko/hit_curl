#include "../inc/hitbtc.h"
#include <fcntl.h>

void  test_secondpart(int count, char **str)
{
  int     fd;
  char    *line;
  char    *for_open;

  if (!(for_open = (char *)malloc(sizeof(char) * 1024)))
    return ;
  strcpy(for_open, "../test_second_part/");
  strcat(for_open, ft_itoa(count));
  if (!(fd = open(for_open, O_RDONLY)))
      return ;
  get_next_line(fd, &line);
  if (fd)
    close(fd);
  if (line == NULL)
    return ;
  str[0] = strdup(line);
  free(line);
}
