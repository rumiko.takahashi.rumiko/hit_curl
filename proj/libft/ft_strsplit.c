/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vbudnik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 21:22:20 by vbudnik           #+#    #+#             */
/*   Updated: 2018/04/19 21:16:33 by vbudnik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t			ft_count_words(char const *s, char c)
{
	size_t	i;

	i = 0;
	while (*s)
	{
		if (*s == c)
			s++;
		while (*s != c)
		{
			if (*(s + 1) == c || *(s + 1) == '\0')
				i++;
			if (*(s + 1) == '\0')
				return (i);
			s++;
		}
		if (*(s + 1) == '\0')
			return (i);
	}
	return (i);
}

static size_t	count_symbols(char const *s, char c)
{
	size_t		i;

	i = 0;
	while (*s == c)
		s++;
	while (*s != c && *s != '\0')
	{
		i++;
		s++;
	}
	return (i);
}

char			**ft_strsplit(char const *s, char c)
{
	char		**arr_ptr;
	size_t		len;
	size_t		i;
	size_t		j;

	if (!s)
		return (NULL);
	j = ft_count_words(s, c);
	i = 0;
	arr_ptr = NULL;
	if (!(arr_ptr = (char **)ft_memalloc(sizeof(s) * j + 1)))
		return (NULL);
	while (j--)
	{
		len = count_symbols(s, c);
		if (!(arr_ptr[i] = ft_memalloc(sizeof(s) * len + 1)))
			return (NULL);
		while (*s == c)
			s++;
		ft_strncpy(arr_ptr[i], s, len);
		s = s + len;
		i++;
	}
	arr_ptr[i] = 0;
	return (arr_ptr);
}
