
#ifndef		HITBTC_H
# define	HITBTC_H

#include "../libft/libft.h"
#include <stdio.h>
#include <curl/curl.h>
#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <curses.h>
#include <curses.h>
#include <ncurses.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <pwd.h>
#include <uuid/uuid.h>


#define H 1200
#define W 1000

# define RED   "\x1B[31m"
# define GRN   "\x1B[32m"
# define YEL   "\x1B[33m"
# define BLU   "\x1B[34m"
# define MAG   "\x1B[35m"
# define CYN   "\x1B[36m"
# define RESET "\x1B[0m"
# define NAME "stas"

/*
** global variable
*/

int		g_f1;
int		g_f2;

// Read params from user

typedef struct	s_search_param
{
	int 		reg; // времено нет
	int 		time; // первого режима
	char		*beep_time;
	int			pr_bl1; // процент превого блока
	int			pr_bl2; // процент второго блока
	char		**search_value_1; // которие ищем или олл первий блок
	char		**search_value_2; // которие ищем или олл второй блок блок
	char		**unsearch_value_1; // исключаем токина из превого блока
	char		**unsearch_value_2; // исключаем токина из второго  блока
}				t_search_param;

typedef struct	s_cutl
{
	char		*str;
	size_t		len;
}				t_curl;

typedef struct	s_json
{
	char	*curl_res;
	char	**json_tokin;

/*
** for TesT
*/
	char	*curl_res_prev;
	int 	len_curl_prev;

/*
finish for TesT
*/

	int		len;
	int 	len_privius;
	int 	const_len;
	char	**ask;
	char 	**last;
	char 	**bid;
	char	**open;
	char	**global_change_ch;
	char	**symbol;
	char	**last_symbol;
	char	**new_coin;
	char	**time;
	char	**symbol_name;
	char 	**const_symbol_name;
	double	*ask_value;
	double 	*last_value;
	double 	*open_value;
	double	*const_value;
	double	*res;
	double	*global_change;
	double	*change;
	double	*change2;
	char	**f1_reset;
	char	**f2_reset;
	char	**old_monet;
	int 	flag_leaks;
	int 	f2;
	int 	o;
}				t_json;

typedef struct s_mlx
{
	void	*mlx;
	void	*win;
	void	*image;
}				t_mlx;

typedef struct	s_math
{
	int		**math;

}				t_math;



void 		beeper_ask(char *str, int f1, int f2, t_json *json, t_search_param	*user);

int 		second_part(t_json *json, int j, t_search_param	*user, int *k, int c);
int 		first_part(t_json *json, int j, t_search_param	*user, int *k);
int   	input_check_1(char *str, t_search_param	*user);
int			input_check_2(char *str, t_search_param	*user);
int			out_check_1(char *str, t_search_param	*user);
int			out_check_2(char *str, t_search_param	*user);

void	init_strin(t_curl *s);
size_t	writefunc(void *ptr, size_t size, size_t nmemb, t_curl *s);
void	get_curl(t_curl s, char **curl_res, char *str);
int		ft_len2darr(char **arr);
void	record(t_json *json, int c);
void	corect_ask(t_json *json, int count);
void	corect_open(t_json *json,int count);
void	corect_last(t_json *json, int count);
int		corect_symbol(t_json *json, int c);
void	repit(t_json *json);
double	comper(double nb1, double nb2);
void	corect_double_arr(double *tmp1, double *tmp2, int len);
void	swap(double **t1, double **t2);
void 	swap_symbol(t_json *json);
void	corect_change(t_json *json);
char	*ft_getline(void);


void	ft_strrev(char *str);


int		sounde(void);
void	create_arrays(t_json *json, int c);
void	copy_arrays(t_json *json, t_search_param	*user);
void	copy_arrays2(t_json *json, t_search_param	*user);
char		**copy_array_glob(char **ar1, char **ar2);


/*
** fork and signal
*/

void	signal_handler(int sig);
int		BMW(char *arg);

/*
** for TesT of second first_part
*/

void  test_secondpart(int count, char **str);


/*
** new coin
*/

void 		swap_symbol(t_json *json);
int  		find_coint(t_json *json);

/*
** for history open and write file
*/

void  write_history(char **str);
void  read_history(char **str);

/*
** for free opein and symbol when count != 0;
*/

void free_arr_json(t_json *json);

/*
** for reading file fot unsearch
*/

char    **read_file_1(void);
char    **read_file_2(void);
void    write_file_1(char *str);
void  	write_file_2(char *str);
void		ft_freearr(char **arr);
int 		ft_free_name(char **arr, int len);
char		*ft_argsjoin(char **args);

/*
** intrface
*/

void	put_param(t_search_param	*user);
void	put_param_bl1(char *str, t_search_param	*user);
void	put_param_bl2(char *str, t_search_param	*user);
void	init_user_struct(t_search_param	*user);
void	test_print_func(t_search_param	*user);
void	put_param(t_search_param	*user);


/*
** find corect index of symbol_n
*/

int   serch(char *str, t_json *json);
char	*get_name(void);

#endif
